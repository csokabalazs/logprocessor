@echo off
IF NOT EXIST "%JAVA_HOME%" (
    IF EXIST C:\cetis SET JAVA_HOME=c:\cetis\java\jre
)

java -jar %~dp0\${project.artifactId}-${project.version}.jar %1 %2