package de.sundn.cs.model;

import lombok.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder(toBuilder = true)
@XmlAccessorType(XmlAccessType.FIELD)
public class ReportEntry implements Comparable<ReportEntry> {
    @XmlAttribute
    private String lengthRate;
    @XmlAttribute
    private Integer length;
    @XmlAttribute
    private String clazz;
    @XmlAttribute
    private String method;
    @XmlAttribute
    private Integer occurrence;
    @XmlAttribute
    private String occurrenceRate;

    @Override
    public int compareTo(ReportEntry o) {
        return o.length - this.length;
    }
}
