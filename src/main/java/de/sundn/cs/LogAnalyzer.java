package de.sundn.cs;


import de.sundn.cs.generator.ReportGenerator;

import javax.xml.bind.JAXBException;
import java.io.IOException;
public class LogAnalyzer {

    public static void main(String[] args) throws IOException, JAXBException {
        if (args.length != 2){
            throw new IllegalStateException("you need two parameters, first if the source of the log files, and second is the source where you want the reports");
        }
        ReportGenerator.generateReport(args[0], args[1]);
    }


}
