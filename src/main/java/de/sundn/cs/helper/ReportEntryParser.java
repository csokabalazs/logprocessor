package de.sundn.cs.helper;

import de.sundn.cs.model.FileWithRegex;
import de.sundn.cs.model.LogEntry;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import de.sundn.cs.model.ReportEntry;

import java.util.*;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ReportEntryParser {
    private static final String SPLIT_CLASSNAME_REGEX = "([ \\]<>-]{2,5})";
    private static final String CUT_METHOD_FROM_VALUE = "[(].";

    private static List<LogEntry> parseLogEntries(FileWithRegex fileWithRegex) {
        log.info("Splitting based on pattern: " + fileWithRegex.getSelectedRegex());
        List<LogEntry> logEntryList = new ArrayList<>();
        String[] logLinesWithoutDate = fileWithRegex.getFileContent().split(fileWithRegex.getSelectedRegex());
        for (String logLine : logLinesWithoutDate) {
            String[] cutClass = logLine.split(SPLIT_CLASSNAME_REGEX, 2);
            if (cutClass.length > 1) {
                String[] cutMethod = cutClass[1].split(CUT_METHOD_FROM_VALUE, 2);
                if (cutMethod.length > 1) {
                    logEntryList.add(new LogEntry(cutClass[0], cutMethod[0], cutMethod[1]));
                } else {
                    logEntryList.add(new LogEntry(cutClass[0], cutMethod[0], ""));
                }
            } else {
                log.warn("Unhandled line={}", logLine);
            }
        }
        return logEntryList;
    }



    private static List<ReportEntry> toReportEntries(List<LogEntry> logEntryList){
        Map<LogEntry, ReportEntry> unsortedMap = new HashMap<>();
        long totalCharacters = 0;
        for (LogEntry logEntry : logEntryList){
            totalCharacters += logEntry.getMethodReturnValue().length();
        }
        for (LogEntry logEntry : logEntryList){
            ReportEntry reportEntry;
            assert totalCharacters != 0;
            if (unsortedMap.containsKey(logEntry)){
                final int length = unsortedMap.get(logEntry).getLength() + logEntry.getMethodReturnValue().length();
                final int occurrence = unsortedMap.get(logEntry).getOccurrence() + 1;
                reportEntry = unsortedMap.get(logEntry)
                                         .toBuilder()
                                         .length(length)
                                         .occurrence(occurrence)
                                         .lengthRate(getLengthRate(totalCharacters, length))
                                         .occurrenceRate(getOccuranceRate(logEntryList.size(), occurrence))
                                         .build();

            } else {
                final int length = logEntry.getMethodReturnValue().length();
                final int occurrence = 1;
                reportEntry = ReportEntry.builder()
                                         .clazz(logEntry.getClassName())
                                         .method(logEntry.getMethodName())
                                         .length(length)
                                         .occurrence(occurrence)
                                         .lengthRate(getLengthRate(totalCharacters, length))
                                         .occurrenceRate(getOccuranceRate(logEntryList.size(), occurrence))
                                         .build();

            }
            unsortedMap.put(logEntry, reportEntry);
        }

        final ArrayList<ReportEntry> reportEntries = new ArrayList<>(unsortedMap.values());
        Collections.sort(reportEntries);
        return reportEntries;
    }

    private static String getLengthRate(double totalCharacters, long length) {
        if (totalCharacters  == 0){
            return "0.00%";
        }
        return String.format("%.2f", (length / totalCharacters) * 100) + "%";
    }

    private static String getOccuranceRate(int totalEntriesSize, int currentEntrySize) {
        return String.format("%.2f", ((double) currentEntrySize / (double) totalEntriesSize) * 100) + "%";
    }

    public static List<ReportEntry> parseReportEntries(FileWithRegex fileWithRegex){
        final List<LogEntry> logEntryList = parseLogEntries(fileWithRegex);
        return toReportEntries(logEntryList);
    }
}
