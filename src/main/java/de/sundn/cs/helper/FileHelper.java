package de.sundn.cs.helper;

import de.sundn.cs.model.FileWithRegex;
import de.sundn.cs.model.Report;
import de.sundn.cs.model.ReportEntry;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileHelper {
    private static final String REPORTS_XML = "/reports/xml/";
    private static final String REPORTS_CSV = "/reports/csv/";
    private static final String CSV_HEADER = "Clazz,Method,Occurance,Occurance Rate,Length,Length Rate\n";

    public static FileWithRegex readFileWithRegex(String logFileName){
        log.info("reading file: " + logFileName);
        StringBuilder sb = new StringBuilder();
        FileWithRegex fileData = new FileWithRegex();
        File logFile = new File(logFileName);
        try (Scanner logScanner = new Scanner(logFile)) {
            List<String> regexes= RegexBuilder.buildRegexCombinations();
            log.info("loading file {} into memory", logFileName);
            while (logScanner.hasNextLine()) {
                final String currentLine = logScanner.nextLine();
                if (fileData.getSelectedRegex() == null)
                    fileData.setSelectedRegex(selectMatchingPattern(regexes, currentLine));
                sb.append(currentLine);
            }
            log.info("loading file {} into memory end", logFileName);
        } catch (FileNotFoundException e) {
            log.error("No such file", e);
        }
        fileData.setFileContent(sb.toString());
        if (fileData.getSelectedRegex() == null)
            throw new RuntimeException("No matching pattern found");

        return fileData;
    }

    private static String selectMatchingPattern(List<String> regexes, String currentLine) {
        for (String regex : regexes) {
            if (currentLine.matches(".*" + regex + ".*")) {
                return regex;
            }
        }
        return null;
    }

    public static void serializeXmlReport(List<ReportEntry> reportEntries,
                                           String filename,
                                           String outputLocation) throws JAXBException, IOException {
        Files.createDirectories(Paths.get(outputLocation + REPORTS_XML));
        final Path xmlFilePath = Paths.get(outputLocation + REPORTS_XML + filename + ".xml");
        log.info("Generating XML report for: {} => {}", filename, xmlFilePath);
        Report data = new Report(reportEntries);
        JAXBContext context = JAXBContext.newInstance(Report.class);
        Marshaller mar = context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar.marshal(data, xmlFilePath.toFile());
    }

    public static void serializeCsvReport(List<ReportEntry> logList, String filename, String outputLocation) throws IOException {
        final Path csvFilePath = Paths.get(outputLocation + REPORTS_CSV + filename + ".csv");
        Files.createDirectories(Paths.get(outputLocation + REPORTS_CSV));
        log.info("Generating CSV report for: {} => {}", filename, csvFilePath);
        try (FileWriter writer = new FileWriter(csvFilePath.toString())) {
            writer.write(CSV_HEADER);
            for (ReportEntry reportEntry : logList) {
                final String csvLine = String.join(",",
                                                   reportEntry.getClazz().replace(",", " "),
                                                   reportEntry.getMethod().replace(",", " "),
                                                   reportEntry.getOccurrence().toString(),
                                                   reportEntry.getOccurrenceRate().replace(",", "."),
                                                   reportEntry.getLength().toString(),
                                                   reportEntry.getLengthRate().replace(",", ".") + "\n");
                writer.write(csvLine);
            }
        } catch (FileNotFoundException e) {
            log.error("CSV file not found. csvFile={}", csvFilePath, e);
        }
    }
}
