package de.sundn.cs.helper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RegexBuilder {
    private static final String DATE_REGEX = "((\\d{4}([-./ ]\\d{2}){2}[-./ ])|((\\d{2}[.-/ ]){2}(\\d{4}[.-/ ]{0,1}|)))";
    private static final String TIME_REGEX = "(\\d{2}[:]){2}\\d{2}[\\.,]\\d{0,3}\\s";
    private static final String THREAD_REGEX = "[\\[(][ a-zA-Z0-9(\\-_.]*[\\])]";
    private static final String STATE_REGEX = "([ A-Z]{2,100}[\\[\\(]| )";
    private static final String CLOSING_TAG_REGEX = "[ -<>]*";

    public static List<String> buildRegexCombinations() {
        log.info("Making regex combinations");
        List<String> regexList = new ArrayList<>();
        regexList.add(THREAD_REGEX);
        regexList.add(STATE_REGEX);

        List<List<String>> permutedPatterns = generatePerm(regexList);
        return permutedPatterns.stream()
                               .map(patternList -> String.join("", patternList))
                               .map(pattern -> DATE_REGEX + TIME_REGEX + pattern + CLOSING_TAG_REGEX)
                               .collect(Collectors.toList());
    }

    private static List<List<String>> generatePerm(List<String> original) {
        if (original.isEmpty()) {
            List<List<String>> result = new ArrayList<>();
            result.add(new ArrayList<>());
            return result;
        }
        String firstElement = original.remove(0);
        List<List<String>> returnValue = new ArrayList<>();
        List<List<String>> permutations = generatePerm(original);
        for (List<String> smallerPermutated : permutations) {
            for (int index = 0; index <= smallerPermutated.size(); index++) {
                List<String> temp = new ArrayList<>(smallerPermutated);
                temp.add(index, firstElement);
                returnValue.add(temp);
            }
        }
        return returnValue;
    }
}
