package de.sundn.cs.generator;

import de.sundn.cs.helper.FileHelper;
import de.sundn.cs.helper.ReportEntryParser;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import de.sundn.cs.model.FileWithRegex;
import de.sundn.cs.model.ReportEntry;
import de.sundn.cs.model.Report;
import org.apache.commons.io.FilenameUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ReportGenerator {
    private static final String LOG_EXTENSION = "log";

    public static void generateReport(String logLocation, String outputLocation) throws IOException, JAXBException {
        File inputFolder = new File(logLocation);
        String[] files = inputFolder.list();

        assert files != null;
        for (String file : files) {
            if (LOG_EXTENSION.equalsIgnoreCase(FilenameUtils.getExtension(file))) {
                FileWithRegex fileWithRegex = FileHelper.readFileWithRegex(logLocation + "/" + file);
                List<ReportEntry> reportEntries = ReportEntryParser.parseReportEntries(fileWithRegex);
                FileHelper.serializeXmlReport(reportEntries, file, outputLocation);
                FileHelper.serializeCsvReport(reportEntries, file, outputLocation);
            }
        }
    }
}
