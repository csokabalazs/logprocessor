# Log analyzer
This tool can be used to create both xml and csv report files from the logs in the chosen directory.

## Usage
1. Build the tool with mvn clean install.
2. Copy the analyze.cmd and the logProcessor-1.0.jar into the directory you want to use it at.
3. Call the analyze.cmd with two parameters:
   1. First parameter is the directory that contains the log files
   2. Second parameter is the directory where you want the reports to be created.
4. After the program has done running, you can find all the csv and xml reports in their own subfolders.